
package logica;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Contiene todo los productos del mini mercado
 * 
 * @author Miguel Ortiz
 */
public class ProductosMercado {
    
    /**
     * Declaracion de los atributos
     */
    private List<Producto> productos;
    json pro=new json("data/","productos.json");
    /**
     * Creacion del constructor vacio
     */
    public ProductosMercado() {
        productos=pro.recurperarObjetos();
    }
    /**
     * agrega productos a la lista
     * @return  retorna true si la funcion agrega el producto
     */
    public boolean agregarProductosNuevos(){
        
        Scanner teclado=new Scanner(System.in);
        
        System.out.println("Ingrese nombre del producto");
        String nombre=teclado.next();
        System.out.println("Ingrese su descripcion");
        String desc=teclado.next();
        System.out.println("Ingrese su cantidad");
        int cantidad=teclado.nextInt();
        System.out.println("Ingrese su categoria");
        String catg=teclado.next();
        
        Producto p=new Producto(nombre,desc,cantidad,catg);
        
        return this.productos.add(p);
    }
    /**
     * muestra los productos
     */
    public void mostrarTodosLosProductos(){
        
        for (Producto p:this.productos) {
            System.out.println("Nombre: "+p.getNombre());
            System.out.println("Descripcion: "+p.getDescripcion());
            System.out.println("Cantidad: "+p.getCantidad());
            System.out.println("Categoria: "+p.getCategoria());
            System.out.println("");
        }
    }
    
}
