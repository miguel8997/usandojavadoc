
package logica;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.List;

public class json {

    private   String RUTA;
    private   String NOMBRE;

    public json(String RUTA,String NOMBRE) {
        this.RUTA=RUTA;
        this.NOMBRE=NOMBRE;
    }

    public boolean almacenarObjetos(List<Producto> piezas) {
        boolean respuesta = false;
        Type listType = new TypeToken<List<Producto>>() {}.getType();
        Gson gson = new Gson();
        String json = gson.toJson(piezas, listType);
        
        try (FileWriter file = new FileWriter(RUTA+NOMBRE)){
            
            file.write(json);
            respuesta = true;
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return respuesta;
    }

    public  List<Producto> recurperarObjetos() {
        Gson gson = new Gson();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(this.RUTA+this.NOMBRE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        List<Producto> productos = gson.fromJson(br, new TypeToken<List<Producto>>() {}.getType());
        
        return productos;     
    }
    
}
