
package logica;

/**
 * 
 * 
 * @author Miguel Ortiz
 */
public class Producto {
    /**
     * Declaracion de los atributos
     */
    private String nombre;
    private String descripcion;
    private int cantidad;
    private String categoria;
    /**
     * 
     * @param nombre nombre del producto
     * @param descripcion descripcion del producto
     * @param cantidad cantidad del productos
     * @param categoria categoria del producto
     */
    public Producto(String nombre, String descripcion, int cantidad, String categoria) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.categoria = categoria;
    }
    /**
     * 
     * @return retorna el nombre del producto
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * 
     * @param nombre se le pasa el nombre del producto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * 
     * @return retorna la descripcion del producto
     */
    public String getDescripcion() {
        return descripcion;
    }
    /**
     * 
     * @param descripcion se le pasa la descripcion del producto
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    /**
     * 
     * @return retorna la cantidad del producto
     */
    public int getCantidad() {
        return cantidad;
    }
    /**
     * 
     * @param cantidad se le pasa la cantidad del producto
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    /**
     * 
     * @return retorna la categoria del producto
     */
    public String getCategoria() {
        return categoria;
    }
    /**
     * 
     * @param categoria se le pasa la categoria del producto
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
    
    
}
